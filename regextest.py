import unittest
import re
import viewengine

class RegexTest(unittest.TestCase):

    def testregex01(self):
        text = "{{something}}this is some {{name}} text. then {{something}} else {{name}}."
        pattern = "[{]{2}([a-zA-Z_][a-zA-Z0-9_.]*)[}]{2}"
        model = {'name':'arun', 'something':'nothing'}
        #pattern = "{{name}}"
        text_util = viewengine.TextUtil()
        result = text_util.bind(text, model)
        self.assertEqual('nothingthis is some arun text. then nothing else arun.', result)

    def testregex02(self):
        text = "this is something"
        pattern = "is"
        matches = re.search(pattern, text)
        pass



if __name__ == "__main__":
    unittest.main()

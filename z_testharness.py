import os
import pathlib
import json
import viewengine
import xml.etree.ElementTree as ET
import xml.dom.minidom as minidom

class Runner:

    def __init__(self):
        self.view_catalog = dict()
        self.model = None

    def load_views(self):
        textfiles = pathlib.Path('.').glob('*.txt')
        for file in textfiles:
            if file.name.startswith('z_view_'):
                view_name = file.stem.replace('z_view_', '')
                text = ''
                with file.open() as f:
                    text = f.read()
                print('view :{0}'.format(view_name))
                self.view_catalog[view_name] = text

    def get_model(self):
        textfiles = pathlib.Path('.').glob('*.txt')
        for file in textfiles:
            if file.name == 'z_model.txt':
                with file.open() as f:
                    modelstring = f.read()
                    print(modelstring)
                    if modelstring != "":
                        self.model = json.loads(modelstring)

    def render_view(self, view_name):
        vw_engine = viewengine.ViewEngine(self.view_catalog)
        output = vw_engine.render_view_by_name(view_name, self.model)
        output = minidom.parseString(output).toprettyxml()
        with open('z_rendered_output.txt','w') as f:
            f.write(output)


if __name__ == '__main__':
    runner = Runner()
    runner.load_views()
    runner.get_model()
    runner.render_view('person')
import unittest
import viewengine


class ViewEngineTests(unittest.TestCase):

    def setUp(self):
        self.view_catalog = dict()
        self.view_catalog['simple_view'] = ViewEngineTests.SIMPLE_VIEW_CONTENT
        self.view_catalog['simple_view_with_model'] = ViewEngineTests.SIMPLE_VIEW_CONTENT_WITH_MODEL
        self.view_catalog['view_with_name_age_state'] = ViewEngineTests.VIEW_CONTENT_WITH_NAME_AGE_STATE
        self.view_catalog['view_with_simple_attribute'] = ViewEngineTests.VIEW_CONTENT_WITH_SIMPLE_ATTRIBUTE
        self.view_catalog['view_with_simple_attribute_placeholder_and_normal_attribute'] = ViewEngineTests.VIEW_CONTENT_WITH_SIMPLE_ATTRIBUTE_PLACEHOLDER_AND_NORMAL_ATTRIBUTE
        self.view_engine = viewengine.ViewEngine(self.view_catalog)

    def test_simple_view_with_no_model(self):
        result = self.view_engine.render_view_by_name('simple_view')
        self.assertEqual('<div>content</div>',result)

    def test_simple_view_with_model_with_first_name_property(self):
        result = self.view_engine.render_view_by_name('simple_view_with_model',{'firstname':'arun'})
        self.assertEqual('<div>arun</div>', result)

    def test_view_with_name_age_state_with_model(self):
        result = self.view_engine.render_view_by_name('view_with_name_age_state',{'firstname':'Arun', 'lastname':'Noronha', 'age': '36', 'state':'butler','hr':'__________________'})
        self.assertEqual('<div>Information<span><span>FirstName : Arun</span></span>__________________<span>LastName: Noronha</span><span>Age : 36</span>__________________<span>State : butler</span>__________________</div>',result)

    def test_view_with_simple_attribute(self):
        result = self.view_engine.render_view_by_name('view_with_simple_attribute', {'type':'text'})
        self.assertEqual('<div type="text">some content</div>', result)

    def test_view_with_simple_attribute_placeholder_and_normal_attribute(self):
        result = self.view_engine.render_view_by_name('view_with_simple_attribute_placeholder_and_normal_attribute', {'typename':'type', 'type':'text'})
        self.assertEqual('<div type="text" other="foo">some content</div>', result)

    SIMPLE_VIEW_CONTENT = """
    <div>content</div>
    """

    SIMPLE_VIEW_CONTENT_WITH_MODEL = """
    <div>{{firstname}}</div>
    """

    VIEW_CONTENT_WITH_NAME_AGE_STATE = """
    <div>
        Information
        <span>
        <span>
        FirstName : {{firstname}}
        </span>
        </span>
        {{hr}}
        <span>
        LastName: {{lastname}}
        </span>
        <span>
        Age : {{age}}
        </span>
        {{hr}}
        <span>
        State : {{state}}
        </span>
        {{hr}}
    </div>
    """

    VIEW_CONTENT_WITH_SIMPLE_ATTRIBUTE = """
    <div type="{{type}}">some content</div>
    """

    VIEW_CONTENT_WITH_SIMPLE_ATTRIBUTE_PLACEHOLDER_AND_NORMAL_ATTRIBUTE = """
    <div type="{{type}}" other="foo">some content</div>
    """


if __name__ == '__main__':
    #suite = unittest.TestLoader().loadTestsFromTestCase(ViewEngineTests)
    suite = unittest.TestLoader().loadTestsFromNames(['test_view_with_simple_attribute_placeholder_and_normal_attribute'], ViewEngineTests)
    unittest.TextTestRunner(verbosity=2).run(suite)

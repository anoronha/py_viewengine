import xml.etree.ElementTree as ET
import xml.dom.minidom as minidom
import re
import collections

class ViewEngine:

    def __init__(self, view_catalog):
        self.view_catalog = view_catalog
        self.text_util = TextUtil()
        self.nonrendertags = ['render-control', 'render-region', 'region-definition']
        self.nonrenderattributes = ['datascope', 'listscope']

    def render_view_by_name(self, viewname, model=None):
        viewstring = None
        if viewname in self.view_catalog:
            viewstring = self.view_catalog[viewname]
        return self.render_view_by_content(viewstring, model)

    def render_view_by_content(self, viewstring, model=None):
        if viewstring is None:
            return None
        viewTree = ET.fromstring(viewstring)
        fragments = []
        self.render_view_by_node(viewTree, model, fragments)
        xmlString = ''.join(fragments)
        return xmlString

    def render_view_by_node(self, view_tree, model=None, fragments=None, regions=None):
        if fragments is None:
            fragments = []
        scope_spec, is_list = self.get_scope_spec(view_tree, model)
        scope = self.get_scope_from_spec(scope_spec, model)

        if is_list and scope is not None:
            for scope_entry in scope.get_underlying_object():
                self.render_view_by_node_for_scope(view_tree, ModelScope(scope_entry), fragments, regions)
        else:
            self.render_view_by_node_for_scope(view_tree, scope, fragments, regions)

    def render_view_by_node_for_scope(self, view_tree, scope, fragments=None, regions=None):

        tag_name = view_tree.tag.strip()
        # rending control blocks
        if tag_name == 'render-control':
            self.render_control_by_node(view_tree, scope, fragments)
            return

        # rendering region-definitions
        if tag_name == 'region-definition':
            region_name = view_tree.attrib['name']
            if regions is None or region_name not in regions:
                raise Exception('region name {0} could not be found because regions was none'.format(region_name))
            region_to_render = regions[region_name]
            self.render_view_by_node(region_to_render, scope, fragments, regions)
            return

        # start tag
        if tag_name not in self.nonrendertags:
            fragments.append('<{0}'.format(tag_name))
            for attrstring in self.process_attributes(view_tree, scope):
                fragments.append(' ')
                fragments.append(attrstring)
            fragments.append('>')

        # text between the start tag and first child
        if view_tree.text is not None:
            fragments.append(self.processtext(view_tree.text.strip(), scope))
        # render children
        for child in view_tree:
            self.render_view_by_node(child, scope, fragments, regions)
        # render close tag
        if tag_name not in self.nonrendertags:
            fragments.append('</{0}>'.format(tag_name))

        # render text between the close tag and next element
        if view_tree.tail is not None:
            fragments.append(self.processtext(view_tree.tail.strip(), scope))

    def get_regions(self, view_tree):
        regions = dict()
        elements = view_tree.findall('render-region')
        if elements is None:
            return regions
        for element in elements:
            region_name = element.attrib['name']
            regions[region_name] = element
        return regions

    def render_control_by_node(self, view_tree, model, fragments, regions=None):
        control_name = self.get_control_name(view_tree)
        control_string = self.view_catalog[control_name]
        control_tree = ET.fromstring(control_string)
        regions = self.get_regions(view_tree)
        propertymaps = self.get_property_maps(view_tree)
        for propertymap in propertymaps:
            model.attach(propertymap[1], model.get(propertymap[0]))
        self.render_view_by_node(control_tree, model, fragments, regions)

    def get_property_maps(self, view_tree):
        propertymaps = []
        elements = view_tree.findall('map-property')
        for element in elements:
            source = element.attrib['source']
            dest = element.attrib['destination']
            propertymaps.append((source, dest))
        return propertymaps

    def get_control_name(self, view_tree):
        if 'name' in view_tree.attrib:
            return view_tree.attrib['name']
        raise Exception('control name not specified')

    def process_attributes(self, view_tree, model):
        for key in view_tree.attrib:
            if key not in self.nonrenderattributes:
                yield '{0}="{1}"'.format(self.processtext(key, model), self.processtext(view_tree.attrib[key], model))

    def processtext(self, text, model):
        return self.text_util.bind(text, model)

    def get_scope_spec(self, view_tree, model):
        if 'datascope' in view_tree.attrib:
            return view_tree.attrib['datascope'], False
        if 'listscope' in view_tree.attrib:
            return view_tree.attrib['listscope'], True
        return None, False

    def get_scope_from_spec(self, scope_spec, model):
        if scope_spec != None:
            return ModelScope(model.get(scope_spec))
        if type(model) is ModelScope:
            return model
        else:
            return ModelScope(model)


class TextUtil:

    def __init__(self):
        self.pattern = "[{]{2}([a-zA-Z_][a-zA-Z0-9_.]*)[}]{2}"

    def bind(self, text, model):
        """
        binds the model data to the text based on placeholders
        :param text: the text to process
        :param model: an object of type ModelScope
        :return:
        """
        matches = re.finditer(self.pattern, text)
        str = ''
        startindex = 0
        for match in matches:
            propbinder = match.group(0)
            propname = match.group(1)
            binderstart = match.start(0)
            binderend = match.end(0)
            print(propname)
            str += text[startindex:binderstart]
            str += '{0}'.format(model.get(propname))
            startindex = binderend
        str += text[startindex:]
        # print(str)
        return str

class ModelScope:

    def __init__(self, model):
        self.model = model
        self.args = dict()
        self.model_util = ModelUtil()

    def __getattr__(self, name):
        return self.get(name)

    def get(self, name):
        if name == "__scope__":
            return self.model
        if type(name) is int:
            return self.model[name]
        value, exists, msg = self.get_property_value(name)
        if exists:
            return value
        return None

    def get_property_value(self, propertypath):
        if propertypath == "__scope__":
            return self.model
        value, exists, msg = self.model_util.get_value(self.model, propertypath)
        if exists:
            return value, exists, msg
        else:
            return self.model_util.get_value(self.args, propertypath)

    def attach(self, propertyname, value):
        self.args[propertyname] = value

    def get_combined_keys(self):
        return set(self.model.keys).union(set(self.args.keys))

    def has_key(self, key):
        return key in self.get_combined_keys()

    def get_underlying_object(self):
        return self.model

class IterScope:
    def __init__(self, model, args):
        self.args = args
        self.model = model
        self.keys = set(model.keys).union(set(args.keys))
        self.index = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self.index >= len(self.keys):
            raise StopIteration
        key = self.keys[self.index]
        self.index = self.index+1
        return key



class ModelUtil:

    def get_value(self, model, path):
        currvalue = model
        dot_path_tokens = path.split('.')
        for dot_path_token in dot_path_tokens:
            field = self.get_field(dot_path_token)
            indexer = self.get_indexer(dot_path_token)
            # if field is None or field.strip() == '':
            #     return None, False, 'field part is empty, path={0}'.format(path)
            # if field not in currvalue:
            #     return None, False, 'field part {0} not in model, path={0}'.format(field,path)
            # currvalue = currvalue[field]
            currvalue, exists, msg = self.get_field_part_value(currvalue,field,path)
            if not exists:
                return currvalue, exists, msg

            if indexer is None or indexer == '':
                pass
            elif indexer.startswith('"'):
                # if the indexer is a string
                indexer = self.get_first([x for x in indexer.split('"') if not(x is None) and x!=''])
            else:
                # if the indexer is an int
                indexer = int(indexer)

            if not(indexer is None) and indexer != '':
                currvalue = currvalue[indexer]

        return currvalue, True, None

    def get_field_part_value(self, model, fieldpart, path):
        if fieldpart is None or fieldpart.strip() == '':
            return None, False, 'field part is empty, path={0}'.format(path)
        if fieldpart not in model:
            return None, False, 'field part {0} not in model, path={0}'.format(fieldpart, path)
        return model[fieldpart], True, None

    def get_field(self, entry):
        indexer_start = entry.find('[')
        if indexer_start == -1:
            return entry
        else:
            return entry[0:indexer_start]

    def get_indexer(self, entry):
        indexer_start = entry.find('[')
        indexer_end = entry.find(']')

        if indexer_start != -1 and indexer_end != -1:
            return entry[indexer_start+1:indexer_end]
        elif indexer_start == -1 and indexer_end != -1:
            raise Exception('indexer start was not found')
        elif indexer_end == -1 and indexer_start != -1:
            raise Exception('indexer end was not found')
        elif indexer_end<indexer_start:
            raise Exception('indexer end was lesser than or equal to the indexer start')
        return None

    def get_first(self, iterable, default=None):
        if iterable:
            for item in iterable:
                return item
        return default


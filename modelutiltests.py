import unittest
import viewengine

class ModelUtilTests(unittest.TestCase):

    def setUp(self):
        self.model_util = viewengine.ModelUtil()

    def test_simple(self):
        model = {'name':'arun'}
        value = self.model_util.get_value(model, 'name')
        self.assertEqual('arun', value)

    def test_nested_property(self):
        model = {'name':'arun', 'address':{'city':'fl'}}
        value = self.model_util.get_value(model, 'address.city')
        self.assertEqual('fl', value)

    def test_nested_list_property(self):
        model = {'addresses':[{'city':'fla'}]}
        value = self.model_util.get_value(model, 'addresses[0].city')
        self.assertEqual('fla', value)

    def test_model_scope_simple(self):
        model = {'name':'arun'}
        value = self.model_util.get_value(viewengine.ModelScope(model), 'name')
        self.assertEqual('arun', value)

    def test_model_scope_nested_list_property(self):
        model = {'addresses':[{'city':'fla'}]}
        value = self.model_util.get_value(viewengine.ModelScope(model), 'addresses[0].city')
        self.assertEqual('fla', value)

if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(ModelUtilTests)
    unittest.TextTestRunner(verbosity=2).run(suite)